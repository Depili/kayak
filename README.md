# Grass Valley Kayak DD mixer controller reverse engineering project

This all applies to the CP-Kayak hardware revision, ie. the 1 M/E controller. All of the "Karrera" revision controllers with 1, 2 or 3 M/E work in similar way and it would be quite easy to also support them, but I don't have such hardware.

I'm using Tinycore linux on the controller for operating system to control the hardware.

## Hardware

The controller is using a KONTRON ETX-mgx baseboard, with a 266Mhz AMD Geode GX processor on it. It is basically a pentium compatible low-power embedded solution from the late 90's. The Kayak DD has 128Mb of ram installed on it. The firmware is on a 128Mb CF-card that is easily replaced.

There are connectors for external VGA display and a ps/2 keyboard on the motherboard inside the unit that one can use to access the BIOS and install a new operating system into the unit. I ended up using tiny core linux as the platform.

### General layout

The hardware is interfaced to the CPU via a PCI bridge controller, PLX PCI9065. A Xilinx CPLD, XC952, is used for glue logic. Both of the switchboard PCBs containing the keys etc seem to be using a unknown FPGA for the IO. The LCD is connected directly to the ETX-mgx baseboard and can be set to mirror the external VGA on the bios. On my unit the backlight for the main LCD seems to be broken, pending more investigations so details about driving the LCD are still bit hazy.

### PLX PCI Bridge

The PLC PCI9056 is the chip responsible for handling communication from the CPU to the peripherals. This is done by memory mapped IO. Initialization data for the PLX chip is held in a EEPROM, so it doesn't need any initialization. The CPLD seems to be connected via the parallel port to the JTAG pins, but it doesn't need reprogramming on each reboot so I haven't fully reverse engineered that part.

The PCI memory regions for the chip are:
1. The chip control registers
2. Unknown IO-ports, probably just chip control registers
3. Memory mapped registers for the switchboard
4. Other memory mapped stuff, currently only the run led is known

#### Run led

The run led is on the 4th PCI region of the PLX chip, at offset 0x2000044 from the base. It is a 16bit register, writing 0 to it disables the run led, writing 1 to it enables it.

#### Memory mapped registers for the switchboard

The switchboard controllers are called lca devices by the firmware. They are handled by some FPGAs that need their firmware to be uploaded after every reboot. The FPGA firmware is located on the partition 5 of the firmware CF card, in the firmware directory. The firmware images used by the different models are:

* Kayak
  1. ds0830
  2. ds0820
* Kayak-2
  1. ds0820
  2. ds0920
  3. ds0930
  4. ds0940
* Kayak-3
  1. ds0952
  2. ds0952
  3. ds0952
  4. ds0952
  5. ds0952

Each device is mapped in region 2. First device is at the base address, rest are offset by 0x10000 each.

The registers, as offsets from the base address are:
* 0x0000 - Group select, 16bit
* 0x0010 - Init, 16bit
* 0x0020 - Done, 16bit
* 0x0030 - Reset, 16bit
* 0x0100 - FW Data, 8bit
* 0x1000 - Flags, 8bit
* 0x1100 - KB mode, 8bit
* 0x1200 - Unknown, seems to be unused
* 0x1300 - KB data, 8bit
* 0x2000 - LCD select, 8bit
* 0x2100 - LCD, 8bit
* 0x2200 - LCD data, 8bit
* 0x2300 - LCD unknown, maybe for the graphical displays on kayak-2 & kayak-3?

##### Firmware upload

1. Write 1 to group select register
2. Write inverted 0x80 to reset register
3. Sleep for a second
4. Write 0x80 to the reset register
5. Poll init register until bit 0 is set
6. Write firmware one byte at a time to the fw data register
7. Poll the done register bit 0 until it becomes set
8. Write 0xFF 0xFF 0xFF 0xFF to the FW data register, one byte at a time
9. Write 0 to group select register

##### Buttons, Leds, ADC's and encoders

These are all controlled by the flags, KB mode and KB data registers. To latch data in and out first set bit 7 (0x80) on the flags registers, wait 10 microseconds, and then reset the bit 7. This is needed to both shift led data out and to read in button state, adc values and encoder values.

* Buttons
  1. Latch data
  2. Write 0xC0 to kb mode register
  3. Read 32 bytes from kb data register
    * A set bit corresponds to a key in the up position, reset bit is a pressed key. Keys are read on the moment you latch the data.
    * Max 256 keys per FPGA
* ADCs
  1. Latch data
  2. Write 0xE0 to kb mode register
  3. Read 16 bytes of data from the kb data register
    * There are 8 ADC channels per FPGA, values are unsigned 16 bit integers
* Encoders
  1. Latch data
  3. Write 0xF0 to kb mode register
  3. Read 8 bytes of data from kb data register
    * On kayak the digipots are 8bit values, that are buffered by the FPGAs. On some of the other controllers the values are 16bit ones.
* Led PWM
  * The controller has two PWM levels for each led group, there can be up to 32 groups per FPGA. The "lowlight" setting controls the brighness of leds that are "off" and the intensity setting controls leds that are "on". The backlights for the small LCDs are also controlled this way.
  * Lowlight
    1. Write 0xA0 to kb mode register
    2. Write 32 bytes to kb data register, each byte is the PWM intensity of a single group
    3. Latch data
  * Intensity:
    1. Write 0x80 to kb mode register
    2. Write 32 bytes to kb data register, each byte is the PWM intensity of a single group
* Leds
  1. Write 0x00 to kb mode register
  2. Write 128 bytes to kb data register
    * This is 32 x 4 bytes, for 32 groups with max of 32 leds each. Each bit controls one led.
  3. Latch data

##### Small character LCDs

These are controlled with the hitachi LCD protocol. Max 8 hitachi LCDs per FPGA, max 8 of some other kind of graphical displays, not present on the kayak I have.

The displays are addressed via the LCD select register, the bit 0 controls command (0) / data (1) access. The firmware writes 0 to the LCD register after setting the address to latch it and then waits 1 microsecond.

* Initialization:
  1. Write the display address (0-7) shifted left one bit (addr << 1) to LCD select register
  2. Write 0 to the LCD register
  3. Wait 1 microsecond
  4. Write 0b00111000 to LCD data register (hitachi initialization code, dual line, 8 bit data bus)
  5. Wait at least 4 milliseconds
  6. Write 0b00111000 to LCD data register (hitachi initialization code, dual line, 8 bit data bus)
  7. Wait at least 110 microseconds
  8. Write 0b00111000 to LCD data register (hitachi initialization code, dual line, 8 bit data bus)
  9. Wait at least 110 microseconds
* Polling the busy flag:
  1. Write the display address (0-7) shifted left one bit (addr << 1) to LCD select register
  2. Write 0 to the LCD register
  3. Wait 1 microsecond
  4. Read one byte from the LCD data register
  5. Busy flag is bit 7 (0x80), if set, wait 10 microseconds and goto 4
* Sending commands:
  1. Write the display address (0-7) shifted left one bit (addr << 1) to LCD select register
  2. Write 0 to the LCD register
  3. Wait 1 microsecond
  4. [Check busy flag, unless you know the display is ok]
  5. Write the command to the LCD_data register
  6. [Check busy flag, send another command...]
* Sending characters
  1. Check busy flag
  1. Write the display address (0-7) shifted left one bit and set bit 1 (addr << 1 | 0x01) to LCD select register
  2. Write 0 to the LCD register
  3. Wait 1 microsecond
  4. Write the character to LCD data register

