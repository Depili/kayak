# Kayak DD




## Firmware

### Part 5 - ds0149.640 - MPK in driver

Alustaa portin 38400 baud

### Run led

Muistimapattuna

wdd 0xc2000044,0x01

Eli PLX:n region3 offset 0x2000044 32bit register


### ds0104.688

TestShowLights funkkarit kiinnostaa, tarttee kattoa mitä luokkafunktiota kutsuvat...

### ds0807.687

tarball of display bmp assets


### ds0805.684

Some led test code and cmd line help for the commands, probably for indicator leds...

### ds0104.688

"CP application"

Has Help__Fv function for some command line help, telnet?

WriteUpkDisplay stuff seems interesting

leds etc are memory mapped

## HW

### COM 1 serial

Provides vxWorks serial terminal, 9600-N-8-1

### Control surface

These handle the keys, leds, lcds, encoders (called digipots in the code) and ADCs (faders)

12CParallelLcaPci class in vxWorks kernel

16bit registers, 8bit data

* base: regGroupSelect
* base + 0x10: regInit
* base + 0x20: regDone
* base + 0x30: regReset
* base + 0x100: regData: firmware load address, 1 usec delay after each written byte
* base + 0x2000: reg select
* base + 0x2100: LCD IR register?
* base + 0x2200: LCD data register

* Set contrast: reg_select = 0x10, IR = 0, data=contrast
* Write data: reg_select = (n * 2)+1, IR = 0, data=char
* Write cmd: reg_select = (n * 2), IR = 0, data=cmd


#### 0xa0000000 controller 1

* Firmware: ds0830.010

#### 0xa0010000 controller 2

* Firmware: ds0820.010

sysSwitchboardFirmwareGet ????
