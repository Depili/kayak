#include "kayak.h"

int main(int argc, char *argv[]){
	char filepath2[] = "/sys/bus/pci/devices/0000:00:09.0/resource2";
	void* baseAddr2 = (void*)BASE_ADDR_2;
	int err, fd2;
	size_t size2;

    // The 16M memory mapped area, contains the FPGA board registers
	void *ptr2 = openRegion(filepath2, baseAddr2, &size2, &fd2);


	printf("Base address, region 2: %p\n", ptr2);
	void *base = ptr2;

	lcaDev dev[2];
	initLcaDevice(&dev[0], base);
	initLcaDevice(&dev[1], base+LCA_OFFSET);

	uint8_t buf[2][2][32];
	int currentBuffer = 0;
	int prevBuffer = 1;
	for (int board = 0; board < NUM_LCA; board++) {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 32; j++) {
				buf[board][i][j] = 0xFF;
			}
		}
	}

	do {
		for (int board = 0; board < NUM_LCA; board++){
			latchData(&dev[board]);

        // Read faders

			*dev[board].kbModeReg = READ_BUTTONS;
			for (int i = 0; i < 32; i++) {
				buf[board][currentBuffer][i] = *dev[board].kbData;
			}

			for (int i = 0; i < 32; i++) {
				uint8_t cur = buf[board][currentBuffer][i];
				uint8_t prev = buf[board][prevBuffer][i];

				if ((cur ^ prev) != 0) {
					uint8_t diff = cur ^ prev;
					for (int j = 0; j < 8; j++) {
						uint8_t mask = 1 << j;
						if ((diff & mask) != 0) {
							printf("Key %02d:%02d:%02d ", board, i, j);
							if ((cur & mask) != 0) {
								printf("up\n");
							} else {
								printf("down\n");
							}
						}
					}
				}

			}
		}
		currentBuffer = (currentBuffer+1) % 2;
		prevBuffer = (prevBuffer+1) % 2;
		usleep(500);
	} while (true);

	close(fd2);
	err = munmap(ptr2, size2);

	if(err != 0){
		printf("Unmapping region 2 failed\n");
		return 1;
	}

	return 0;
}