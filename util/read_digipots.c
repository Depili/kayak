#include "kayak.h"

int main(int argc, char *argv[]){
	char filepath2[] = "/sys/bus/pci/devices/0000:00:09.0/resource2";
	void* baseAddr2 = (void*)BASE_ADDR_2;
	int err, fd2;
	size_t size2;

    // The 16M memory mapped area, contains the FPGA board registers
	void *ptr2 = openRegion(filepath2, baseAddr2, &size2, &fd2);


	printf("Base address, region 2: %p\n", ptr2);
	void *base = ptr2;

	lcaDev dev;
	initLcaDevice(&dev, base);

	do {
		lcaDev dev;
		initLcaDevice(&dev, base);
		latchData(&dev);

        // Read faders
		uint8_t buf[8];

		*dev.kbModeReg = READ_DIGIPOT;
		for (int i = 0; i < 8; i++) {
			buf[i] = *dev.kbData;
		}

		printf("\033[2J");
		printf("Digipots:\n");
		for (int i = 0; i < 4; i++) {
			printf("\t%d: 0x%0.2X\t", i, buf[i]);
		}
		printf("\n");

		usleep(50*1000);
	} while (true);

	close(fd2);
	err = munmap(ptr2, size2);

	if(err != 0){
		printf("Unmapping region 2 failed\n");
		return 1;
	}

	return 0;
}