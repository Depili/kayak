#ifndef KAYAK_H
#define KAYAK_H

#include <stdio.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#define RUN_LED_OFFSET 0x2000044
#define BASE_ADDR_2 0xa0000000
#define BASE_ADDR_3 0xb0000000

#define LCA_OFFSET 0x10000

#define NUM_LCA 2
#define FIRMWARE_0 "ds0830.010"
#define FIRMWARE_1 "ds0820.010"
#define FIRMWARE_SIZE 97652

// 16bit registers
#define LCA_GROUP_SEL 0x0
#define LCA_REG_INIT  0x10
#define LCA_REG_DONE  0x20
#define LCA_REG_RESET 0x30
// 8bit register
#define LCA_REG_DATA  0x100

// 8bit registers
#define LCA_FLAGS     0x1000
#define LCA_KB_MODE   0x1100
#define LCA_SOME_PTR3 0x1200
#define LCA_KB_DATA   0x1300
#define LCA_LCD_SEL   0x2000
#define LCA_LCD       0x2100
#define LCA_LCD_DATA  0x2200
#define LCA_LCD_GFX   0x2300

#define GROUP_SELECT 1
#define INIT_MASK 1
#define DONE_MASK 1
#define RESET_MASK 0x80

#define WRITE_LED_BITMAP    0x00
#define WRITE_LED_INTENSITY 0x80
#define WRITE_LED_LOWLIGHT  0xA0
#define READ_BUTTONS        0xC0
#define READ_FADERS         0xE0
#define READ_DIGIPOT        0xF0

// Various bitmap lengths, per LCA device
#define LED_DATA_LEN     128
#define LED_LOWLIGHT_LEN  32
#define LED_INTENSITY_LEN 32
#define DIGIPOT_LEN        8
#define FADER_LEN         16
#define BUTTONS_LEN       32

// Struct for holding information about one "lca device"
typedef struct lcaDevice {
	volatile uint16_t *groupSelReg;
	volatile uint16_t *initReg;
	volatile uint16_t *doneReg;
	volatile uint16_t *resetReg;
	volatile char *dataReg;

	volatile uint8_t *flagReg;
	volatile uint8_t *kbModeReg;
	volatile uint8_t *somePtr3;
	volatile uint8_t *kbData;
	volatile uint8_t *lcdSel;
	volatile uint8_t *lcd;
	volatile uint8_t *lcdData;
	volatile uint8_t *lcdGfx;
} lcaDev;

// Memory map one PCI region from sysfs
void *openRegion(char *, void *, size_t *, int *);

// Set the various register addresses as offsets from the base address
void initLcaDevice(lcaDev *, void *);

// Latch new data in and out
void latchData(lcaDev *);

// LCD line length
const uint8_t lcdLineLen(uint8_t lcd);

// FPGA 0:
//  0 - buttons white
// 14 - Logo blue
// 15 - Lcd backlight white

// FPGA 1:
// 0 - Delegate white
// 1 - Delegate red
// 2 - Key bus white
// 3 - Key bus red
// 4 - PGM white
// 5 - PGM red
// 6 - PST white
// 7 - PST red
// 8 - Keyers white
// 9 - transition white
// 10 - Effects red
// 11 - Effects green
// 12 - Effects blue
// 13 - Effects white
// 14 - Indicators blue
// 15 - LCD backlight white


#endif /* KAYAK_H */
