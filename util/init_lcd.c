#include "kayak.h"

static void pollLcdBusy(lcaDev *dev, int lcd) {
	int wait = 0;
	*dev->lcdSel = lcd * 2;
	*dev->lcd = 0;
	usleep(1);
	while ((*dev->lcdData & 0x80) != 0) {
		usleep(10);
		if (wait++ > 1000) {
			printf("Error: display busy flag didn't clear!\n");
			exit(1);
		}
	}
}

int main(int argc, char *argv[]){
	char filepath2[] = "/sys/bus/pci/devices/0000:00:09.0/resource2";
	void* baseAddr2 = (void*)BASE_ADDR_2;
	int err, fd2;
	size_t size2;

    // The 16M memory mapped area, contains the FPGA board registers
	void *ptr2 = openRegion(filepath2, baseAddr2, &size2, &fd2);


	printf("Base address, region 2: %p\n", ptr2);
	void *base = ptr2;

	lcaDev dev[2];
	initLcaDevice(&dev[0], base);
	initLcaDevice(&dev[1], base+LCA_OFFSET);

	char text[] = "Hacked by Depili";
	char info[16];

	// Set contrast
	*dev[0].lcdSel = 0x10;
	*dev[0].lcd = 0;
	*dev[0].lcdData = 0x77;
	*dev[1].lcdSel = 0x10;
	*dev[1].lcd = 0;
	*dev[1].lcdData = 0x77;


	for (int board = 0; board < NUM_LCA; board++){
		// Init the displays
		for (int lcd = 0; lcd < 8; lcd++) {
			if (lcdLineLen((board*16)+lcd) != 0) {
				*dev[board].lcdSel = lcd * 2;
				*dev[board].lcd = 0;
				usleep(1);
				// CMD: Init, 8bit bus, dual line, 7x8 font
				*dev[board].lcdData = 0b00111000;
				usleep(5 * 1000);
				*dev[board].lcdData = 0b00111000;
				usleep(150);
				*dev[board].lcdData = 0b00111000;
				usleep(150);
				*dev[board].lcdData = 0b00111000;
				usleep(150);
				// CMD: enable display; turn display off, turn cursor off, turn blink offf
				*dev[board].lcdData = 0b00001000;
				usleep(150);
				// CMD: Clear display
				*dev[board].lcdData = 0b00000001;
				usleep(150);
				// CMD set cursor move, increment on each write, do not shift display
				*dev[board].lcdData = 0b00000110;
			}
		}

		printf("Display init done\n");

		// Try to write something...
		for (int lcd = 0; lcd < 8; lcd++) {
			if (lcdLineLen((board*16)+lcd) != 0) {
				*dev[board].lcdSel = lcd * 2;
				*dev[board].lcd = 0;
				usleep(1);
				pollLcdBusy(&dev[board], lcd);
				// CMD: Init, 8bit bus, dual line, 7x8 font
				*dev[board].lcdData = 0b00111000;
				pollLcdBusy(&dev[board], lcd);
				// Turn the display on
				*dev[board].lcdData = 0b00001100;
				pollLcdBusy(&dev[board], lcd);
				*dev[board].lcdData = 0b00000110;
				pollLcdBusy(&dev[board], lcd);


				printf("Writing Display %d row 1\n", lcd);
				// Write something finally?

				// First row
				// Return the cursor to the home position
				*dev[board].lcdSel = lcd * 2;
				*dev[board].lcd = 0;
				usleep(1);
				*dev[board].lcdData = 0b00000010;
				pollLcdBusy(&dev[board], lcd);
				for (int c = 0; c < 16; c++) {
					*dev[board].lcdSel = (lcd * 2) + 1;
					*dev[board].lcd = 0;
					usleep(5);
					*dev[board].lcdData = text[c];
					pollLcdBusy(&dev[board], lcd);
				}
				printf("Writing Display %d row 2\n", lcd);

				// Second row
				// CMD: set DRAM address 0b0100000
				*dev[board].lcdSel = lcd * 2;
				*dev[board].lcd = 0;
				usleep(5);
				*dev[board].lcdData = 0b11000000;
				pollLcdBusy(&dev[board], lcd);


				int l = snprintf(info, 16, "D: %02d B: %d",lcd, board);
				for (int c = 0; c < l; c++) {
					*dev[board].lcdSel = (lcd * 2) + 1;
					*dev[board].lcd = 0;
					usleep(1);
					*dev[board].lcdData = info[c];
					pollLcdBusy(&dev[board], lcd);
				}

			}
		}
	}

	close(fd2);
	err = munmap(ptr2, size2);

	if(err != 0){
		printf("Unmapping region 2 failed\n");
		return 1;
	}

	return 0;
}