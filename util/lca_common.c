#include "kayak.h"

void initLcaDevice(lcaDev *dev, void *base) {
	dev->groupSelReg = base + LCA_GROUP_SEL;
	dev->initReg = base + LCA_REG_INIT;
	dev->doneReg = base + LCA_REG_DONE;
	dev->resetReg = base + LCA_REG_RESET;
	dev->dataReg = base + LCA_REG_DATA;
	dev->flagReg = base + LCA_FLAGS;
	dev->kbModeReg = base + LCA_KB_MODE;
	dev->somePtr3 = base + LCA_SOME_PTR3;
	dev->kbData = base + LCA_KB_DATA;
	dev->lcdSel = base + LCA_LCD_SEL;
	dev->lcd = base + LCA_LCD;
	dev->lcdData = base + LCA_LCD_DATA;
	dev->lcdGfx = base + LCA_LCD_GFX;
}

void *openRegion(char *path, void *baseAddr, size_t *size, int *fd) {
	int err;
	struct stat statbuf;

	*fd = open(path, O_RDWR | O_SYNC);

	if(*fd < 0){
		printf("Error: could not open: %s\n",path);
		exit(1);
	}

	err = fstat(*fd, &statbuf);
	if(err < 0){
		printf("Error: could not stat: %s\n",path);
		exit(1);
	}
	*size = statbuf.st_size;
	void *ptr = mmap(baseAddr,*size,
		PROT_READ|PROT_WRITE,MAP_SHARED,
		*fd,0);
	if(ptr == MAP_FAILED){
		printf("Mapping region 2 failed\n");
		exit(1);
	}
	return ptr;
}

void latchData(lcaDev *dev) {
	usleep(10);
	*dev->flagReg = *dev->flagReg | 0x80;
	usleep(10);
	*dev->flagReg = *dev->flagReg & 0x7f;
}

const uint8_t lcdLineLen(uint8_t lcd) {
	switch (lcd) {
		case 0:
			return 16;
		case 16:
			return 40;
		case 17:
			return 16;
		default:
			return 0;
	}
}