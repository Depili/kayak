#include "kayak.h"

int main(int argc, char *argv[]){
    char filepath3[] = "/sys/bus/pci/devices/0000:00:09.0/resource3";
    void* baseAddr3 = (void*)BASE_ADDR_3;
    size_t size3;
    int err;

    if (argc < 2) {
        printf("Usage: %s (0|1)\n", argv[0]);
        return 0;
    }

    uint32_t value = 0;

    if (strcmp(argv[1], "1") == 0) {
        value = 1;
    }


    // The 64M memory mapped area, contains the run led
    int fd3 = open(filepath3, O_RDWR | O_SYNC);

    if(fd3 < 0){
        printf("\n\"%s \" could not open\n",
               filepath3);
        exit(1);
    }

    struct stat statbuf;
    err = fstat(fd3, &statbuf);
    if(err < 0){
        printf("\n\"%s \" could not open\n",
                       filepath3);
        exit(2);
    }
    size3 = statbuf.st_size;

    void *ptr3 = mmap(baseAddr3,size3,
            PROT_READ|PROT_WRITE,MAP_SHARED,
            fd3,0);
    if(ptr3 == MAP_FAILED){
        printf("Mapping region 3 failed\n");
        return 1;
    }

    volatile uint32_t *runLed = ptr3 + RUN_LED_OFFSET;


    printf("Base address, region 3: %p\n", ptr3);
    printf("Run led address: %p\n", runLed);
    printf("Setting led to: %d\n", value);
    *runLed = value;

    close(fd3);

    err = munmap(ptr3, size3);

    if(err != 0){
        printf("Unmapping region 3 failed\n");
        return 1;
    }

    return 0;
}