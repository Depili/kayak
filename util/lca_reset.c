#include "kayak.h"


uint8_t *readFirmware(char * path) {
    size_t size;

    printf("Opening %s\n", path);
    FILE *fw = fopen(path, "r");
    if (fw == NULL){
        printf("Error opening %s\n", path);
        exit(1);
    }
    printf("Firmware %s opened\n", path);
    fseek(fw, 0, SEEK_END);

    size = ftell(fw);
    rewind(fw);


    if (size != FIRMWARE_SIZE) {
        printf("Firmware size missmatch, expected: %d got %d\n",FIRMWARE_SIZE, size);
        exit(1);
    }
    printf("Firmware: %s size: %d\n", path, size);

    uint8_t *fwData = malloc(size);

    if (fwData == NULL){
        printf("Out of memory allocaring firmware buffer\n");
        exit(1);
    }

    size_t n = fread(fwData, 1, size, fw);
    if (n != size) {
        printf("Not all of the firmware was read into the buffer!\n");
        printf("Size: %d read: %d\n", size, n);
        exit(1);
    }

    fclose(fw);
    printf("Firmware %s read successfully!\n", path);
    return fwData;

}

int main(int argc, char *argv[]){
    char filepath2[] = "/sys/bus/pci/devices/0000:00:09.0/resource2";
    void* baseAddr2 = (void*)BASE_ADDR_2;
    int err, fd2;
    size_t size2;

    uint8_t *fw0Data = readFirmware(FIRMWARE_0);
    uint8_t *fw1Data = readFirmware(FIRMWARE_1);

    // The 16M memory mapped area, contains the FPGA board registers
    void *ptr2 = openRegion(filepath2, baseAddr2, &size2, &fd2);


    printf("Base address, region 2: %p\n", ptr2);
    printf("Reseting LCA devices:\n");

    uint16_t resetMask = RESET_MASK;
    uint16_t doneMask = DONE_MASK;

    for (uint i = 0; i < NUM_LCA; i++) {
        void *base = ptr2 + (i*LCA_OFFSET);

        lcaDev dev;
        initLcaDevice(&dev, base);

        printf(" -> Device: %d base address %p\n", i, base);
        printf("\tGroup select:   %p\n", dev.groupSelReg);
        printf("\tInit register:  %p\n", dev.initReg);
        printf("\tDone register:  %p\n", dev.doneReg);
        printf("\tReset register: %p\n", dev.resetReg);
        printf("\tData register:  %p\n", dev.dataReg);
        printf("\tFlags register: %p\n", dev.flagReg);
        printf("\tKB mode reg:    %p\n", dev.kbModeReg);
        printf("\tSome ptr3:      %p\n", dev.somePtr3);
        printf("\tKB data:        %p\n", dev.kbData);
        printf("\tLCD select:     %p\n", dev.lcdSel);
        printf("\tLCD register:   %p\n", dev.lcd);
        printf("\tLCD data:       %p\n", dev.lcdData);
        printf("\tLCD gfx:        %p\n", dev.lcdGfx);
        if (i == 0) {
            printf("\tFirmware file:  %s\n", FIRMWARE_0);
        } else {
            printf("\tFirmware file:  %s\n", FIRMWARE_1);
        }


        // ProgramPrepare here
        *dev.groupSelReg = GROUP_SELECT;
        *dev.resetReg =  ~resetMask;
        sleep(2);
        *dev.resetReg = resetMask;
        for (int j = 0; j < 5; j++) {
            if ((*dev.initReg & INIT_MASK) == INIT_MASK) {
                break;
            }
            printf("Waiting for device to reset...\n");
            sleep(1);
        }
        if ((*dev.initReg & INIT_MASK) != INIT_MASK) {
            printf("Device reset failed!\n");
            return 1;
        }

        usleep(4);

        bool done = ((*dev.doneReg & doneMask) == doneMask);
        if (done) {
            printf("Device is DONE\n");
            continue;
        } else {
            printf("Device is not DONE\n");
        }

        // Programming goes here....

        for (int j = 0; j < FIRMWARE_SIZE; j++) {
            if (i == 0) {
                *dev.dataReg = fw0Data[j];
            } else if (i == 1) {
                *dev.dataReg = fw1Data[j];
            }
            // usleep(1);
        }
        printf("Wrote the firmware...\n");

        // Close goes here...

        for (size_t j = 0; j < 5; j++) {
            done = ((*dev.doneReg & doneMask) == doneMask);
            if (done) {
                break;
            }
            sleep(1);
        }

        if (!done) {
            printf("Not all done after waiting!\n");
            exit(1);
        }

        for (int j = 0; j < 4; j++) {
            *dev.dataReg = 0xFF;
        }

        *dev.groupSelReg = 0;

        printf("FPGA %d programmed\n", i);
        sleep(1);
    }

    close(fd2);
    err = munmap(ptr2, size2);

    if(err != 0){
        printf("Unmapping region 2 failed\n");
        return 1;
    }

    return 0;
}