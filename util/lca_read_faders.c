#include "kayak.h"

int main(int argc, char *argv[]){
	char filepath2[] = "/sys/bus/pci/devices/0000:00:09.0/resource2";
	void* baseAddr2 = (void*)BASE_ADDR_2;
	int err, fd2;
	size_t size2;

    // The 16M memory mapped area, contains the FPGA board registers
	void *ptr2 = openRegion(filepath2, baseAddr2, &size2, &fd2);


	printf("Base address, region 2: %p\n", ptr2);

	do {
		for (int i = 0; i < NUM_LCA; i++) {
			void *base = ptr2 + (i*LCA_OFFSET);

			lcaDev dev;
			initLcaDevice(&dev, base);
			latchData(&dev);

        // Read faders
			uint8_t buf[16];

			*dev.kbModeReg = READ_FADERS;
			for (int j = 0; j < 16; j++) {
				buf[j] = *dev.kbData;
			}

			uint16_t *values = (uint16_t *)&buf;

			printf("\033[2J");
			if (i == 1) {
				printf("Mapped fader values:\n");
				printf("\t0x0100: %04u\n", values[0]);
				printf("\t0x5700: %04u\n", values[2]);
				printf("\t0x5701: %04u\n", values[3]);
				printf("\t0x5702: %04u\n", values[1]);
			}
		}
		usleep(50*1000);
	} while (true);

	close(fd2);
	err = munmap(ptr2, size2);

	if(err != 0){
		printf("Unmapping region 2 failed\n");
		return 1;
	}

	return 0;
}