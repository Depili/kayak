#include "kayak.h"

static const uint8_t numLeds[][32] = {
    {21,  0,  0,  0,  0,  0,  0,  0,    0,  0,   0,  0,  0,  0,  1, 1,    0, 0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0, 0},
    {16, 16, 16, 16, 16, 16, 16, 16,    23, 26, 24, 24, 24, 24, 11, 1,    0, 0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0, 0}
};


int main(int argc, char *argv[]){
	char filepath2[] = "/sys/bus/pci/devices/0000:00:09.0/resource2";
	void* baseAddr2 = (void*)BASE_ADDR_2;
	int err, fd2;
	size_t size2;

    // The 16M memory mapped area, contains the FPGA board registers
	void *ptr2 = openRegion(filepath2, baseAddr2, &size2, &fd2);


	printf("Base address, region 2: %p\n", ptr2);


	for (int i = 0; i < NUM_LCA; i++) {
        void *base = ptr2 + (i*LCA_OFFSET);

        lcaDev dev;
        initLcaDevice(&dev, base);


        *dev.kbModeReg = WRITE_LED_BITMAP;
        for (int group = 0; group < 32; group++) {
            for (int j = 0; j < 4; j++) {
                *dev.kbData = 0xFF;
            }
        }




        *dev.kbModeReg = WRITE_LED_INTENSITY;
        for (int j = 0; j < 32; j++) {
            *dev.kbData = 0x00;
        }

        usleep(10);

        *dev.kbModeReg = WRITE_LED_LOWLIGHT;
        for (int j = 0; j < 32; j++) {
            *dev.kbData = 0x00;
        }

        latchData(&dev);
    }

    do {
        for (int i = 0; i < NUM_LCA; i++) {
            void *base = ptr2 + (i*LCA_OFFSET);

            lcaDev dev;
            initLcaDevice(&dev, base);

            for (int group = 0; group < 32; group++) {
                if (numLeds[i][group] == 0) {
                    continue;
                }

                uint32_t leds = 1;
                uint8_t *ledData = (uint8_t *)&leds;

                for (int bitPos = 0; bitPos < numLeds[i][group]; bitPos++) {
                    leds = 1 << bitPos;

                    *dev.kbModeReg = WRITE_LED_BITMAP;
                    for (int group = 0; group < 32; group++) {
                        for (int j = 0; j < 4; j++) {
                            *dev.kbData = ledData[j];
                        }
                    }


                    *dev.kbModeReg = WRITE_LED_INTENSITY;
                    for (int j = 0; j < 32; j++) {
                        if (j == group) {
                            *dev.kbData = 0xFF;
                        } else {
                            *dev.kbData = 0x00;
                        }
                    }

                    latchData(&dev);
                    usleep(50 * 1000);
                }
            }

            *dev.kbModeReg = WRITE_LED_INTENSITY;
            for (int j = 0; j < 32; j++) {
                *dev.kbData = 0x00;
            }
            latchData(&dev);
        }
    } while (true);


    close(fd2);
    err = munmap(ptr2, size2);

    if(err != 0){
      printf("Unmapping region 2 failed\n");
      return 1;
  }

  return 0;
}