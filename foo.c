#include <stdio.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>

#define RUN_LED_OFFSET 0x2000044
#define BASE_ADDR_2 0xa0000000
#define BASE_ADDR_3 0xb0000000

#define LCA_OFFSET  0x10000

// 16bit registers
#define LCA_GROUP_SEL 0x0
#define LCA_REG_INIT  0x10
#define LCA_REG_DONE  0x20
#define LCA_REG_RESET 0x30
// 8bit register
#define LCA_REG_DATA  0x100

// 8bit registers
#define LCA_FLAGS     0x1000
#define LCA_KB_MODE   0x1100
#define LCA_SOME_PTR3 0x1200
#define LCA_KB_DATA   0x1300
#define LCA_LCD_SEL   0x2000
#define LCA_LCD       0x2100
#define LCA_LCD_DATA  0x2200
#define LCA_LCD_GFX   0x2300

#define GROUP_SELECT 1
#define INIT_MASK 1
#define DONE_MASK 1
#define RESET_MASK 0x80

#define WRITE_LED_BITMAP    0x00
#define WRITE_LED_INTENSITY 0x80
#define WRITE_LED_LOWLIGHT  0xA0
#define READ_BUTTONS        0xC0
#define READ_FADERS         0xE0
#define READ_DIGIPOT        0xF0

// Various bitmap lengths, per LCA device
#define LED_DATA_LEN     128
#define LED_LOWLIGHT_LEN  32
#define LED_INTENSITY_LEN 32
#define DIGIPOT_LEN        8
#define FADER_LEN         16
#define BUTTONS_LEN       32

int main(int argc, char *argv[]){
    char filepath2[] = "/sys/bus/pci/devices/0000:00:09.0/resource2";
    void* baseAddr2 = (void*)BASE_ADDR_2;

    char filepath3[] = "/sys/bus/pci/devices/0000:00:09.0/resource3";
    void* baseAddr3 = (void*)BASE_ADDR_3;
    size_t size2, size3;
    int err;

    // The 16M memory mapped area, contains the FPGA board registers
    int fd2 = open(filepath2, O_RDWR | O_SYNC);

    if(fd2 < 0){
        printf("\n\"%s \" could not open\n",
               filepath2);
        exit(1);
    }

    // The 64M memory mapped area, contains the run led
    int fd3 = open(filepath3, O_RDWR | O_SYNC);

    if(fd3 < 0){
        printf("\n\"%s \" could not open\n",
               filepath3);
        exit(1);
    }

    struct stat statbuf;
    err = fstat(fd3, &statbuf);
    if(err < 0){
        printf("\n\"%s \" could not open\n",
                       filepath3);
        exit(2);
    }
    size3 = statbuf.st_size;
    void *ptr3 = mmap(baseAddr3,size3,
            PROT_READ|PROT_WRITE,MAP_SHARED,
            fd3,0);
    if(ptr3 == MAP_FAILED){
        printf("Mapping region 3 failed\n");
        return 1;
    }

    err = fstat(fd2, &statbuf);
    if(err < 0){
        printf("\n\"%s \" could not open\n",
                       filepath2);
        exit(2);
    }
    size2 = statbuf.st_size;
    void *ptr2 = mmap(baseAddr2,size2,
            PROT_READ|PROT_WRITE,MAP_SHARED,
            fd2,0);
    if(ptr2 == MAP_FAILED){
        printf("Mapping region 2 failed\n");
        return 1;
    }

    volatile uint32_t *runLed = ptr3 + RUN_LED_OFFSET;


    printf("Base address, region 2: %p\n", ptr2);
    printf("Base address, region 3: %p\n", ptr3);
    printf("Run led address: %p\n", runLed);

    printf("Blinking led?\n");
    for (int i = 0; i < 10; i++) {
        *runLed = 0x1;
        sleep(1);
        *runLed = 0x0;
        sleep(1);
        printf(".");
    }
    printf("\n");

    close(fd2);
    close(fd3);

    err = munmap(ptr2, size2);

    if(err != 0){
        printf("Unmapping region 2 failed\n");
        return 1;
    }

    err = munmap(ptr3, size3);

    if(err != 0){
        printf("Unmapping region 3 failed\n");
        return 1;
    }

    return 0;
}